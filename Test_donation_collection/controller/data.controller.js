const dataModel = require('../models/data.model');

exports.test = function(req, res){
    res.send('Test berhasil!')
};

exports.new_data = async (req, res) => {
    
    let data = new dataModel({
        nama: req.body.nama,
        target: req.body.target,
        terkumpul: req.body.terkumpul
        }
        );

        try {
            const savedData = await data.save();
            res.send(savedData)
            // res.json({message: 'Input data success'})
            // res.send({data: data._id});
        } catch (error) {
            res.status(400).send({message: 'error'});
        }
    

    // user.save(function(err){
    //     if(err) {
    //         return next(err);
    //     }
    //     res.json({message: 'Input new user success'})

    // })
};

exports.data_list = function(req, res){
    dataModel.find({}, function(err, dataProduct){
        if(err) {
            return next(err);
        }
        res.json({data: dataProduct})
    })
};

exports.data_one = function(req, res){
    dataModel.findById(req.params.id, function(err, dataProduct){
        if(err) {
            return next(err);
        }
        res.json({data: dataProduct})
    })
};

exports.data_topup = function (req, res) {
    dataModel.findByIdAndUpdate(req.params.id, {$inc: { terkumpul: +req.body.terkumpul}}, function (err, product) {
        if (err) return next(err);
        res.send('Data udpated.');
    });
};

exports.data_delete = function (req, res) {
    dataModel.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Data deleted successfully!');
    })
};

