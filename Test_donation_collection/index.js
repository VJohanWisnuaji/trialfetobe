const express = require('express');
const bodyParser = require('body-parser');
const dataRoute = require('./route/data.route');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');


mongoose.connect('mongodb://localhost:27017/store?readPreference=primary&appname=MongoDB%20Compass&ssl=false', {useNewUrlParser: true});
mongoose.Promise = global.Promise;
const connection = mongoose.connection;
connection.on('error', console.error.bind(console, 'MongoDB connection error'));

var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  }


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors(corsOptions));

app.use('/database', dataRoute);


let port = 8083;

app.listen(port, ()=> {console.log('Server is running on port '+port)});
