const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let dataSchema = new Schema({
    nama: {
        type: String,
        required: true
},
    target: {
        type: Number,
        required: true
},
    terkumpul: {
        type: Number,
        default: 0
}
}
);

module.exports = mongoose.model('donation', dataSchema);