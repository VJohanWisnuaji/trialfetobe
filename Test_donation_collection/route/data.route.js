const express = require('express');
const router = express.Router();
const data_controller = require('../controller/data.controller');

router.get('/test', data_controller.test);
router.post('/inputdata', data_controller.new_data);
router.get('/listdata', data_controller.data_list);
router.get('/:id', data_controller.data_one);
router.put('/:id/topup', data_controller.data_topup);
router.delete('/:id/delete', data_controller.data_delete);


module.exports = router;
