const dataModel = require('../models/data.model');
const router = require('express').Router();
const {registerValidation, loginValidation} = require('../validation');
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs');


exports.test = function(req, res){
    res.send('Test berhasil!')
};

exports.user_create = async (req, res) => {
    //validate data before making user
    // const {error} = schema.validate(req.body);
    const {error} = registerValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message)

    //check if email already exist
    const emailExist = await dataModel.findOne({email: req.body.email});
    if (emailExist) return res.status(400).send('email already exist')

    //hash passwords
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt); 

    let user = new dataModel({
        email: req.body.email,
        password: hashPassword,
        confirmPassword: req.body.confirmPassword,
        fullName: req.body.fullName,
        username: req.body.username,
        unQuest: req.body.unQuest,
        unAns: req.body.unAns
        }
        );

        try {
            const savedUser = await user.save();
            // res.send(savedUser)
            // res.json({message: 'Input user success'})
            res.send({user: user._id});
        } catch (error) {
            res.status(400).send({message: 'error'});
        }
    

    // user.save(function(err){
    //     if(err) {
    //         return next(err);
    //     }
    //     res.json({message: 'Input new user success'})

    // })
};

exports.user_list = function(req, res){
    dataModel.find({}, function(err, dataProduct){
        if(err) {
            return next(err);
        }
        res.json({data: dataProduct})
    })
};

exports.user_one = function(req, res){
    dataModel.findById(req.params.id, function(err, dataProduct){
        if(err) {
            return next(err);
        }
        res.json({data: dataProduct})
    })
};

exports.user_update = function (req, res) {
    dataModel.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
        if (err) return next(err);
        res.send('User udpated.');
    });
};

exports.user_delete = function (req, res) {
    dataModel.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('User deleted successfully!');
    })
};

//login
exports.user_login = async (req,res) => {
    const {error} = loginValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message) ;

    const user = await dataModel.findOne({email: req.body.email});
    if (!user) return res.status(400).send('email not exist');

    const validPass = await bcrypt.compare(req.body.password, user.password);
    if (!validPass) return res.status(400).send('PASSword WRONG');

    //create and assign token
    const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET, {expiresIn: "1h"});
    res.header('auth-token', token).send(token)

    // res.send('login success');
}