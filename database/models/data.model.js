const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let dataSchema = new Schema({
    email: {
        type: String,
        required: true,
        min: 6,
        max: 255
},
    password: {
        type: String,
        required: true,
        min: 6,
        max: 255
},
    confirmPassword: {
        type: String,
        required: true,
        min: 6,
        max: 255
},
    fullName: {
        type: String,
        required: true,
        min: 6,
        max: 255
},
    username:{
        type: String,
        required: true,
        min: 6,
        max: 255
},
    unQuest: {type: String,required: true},
    unAns: {type: String,required: true}
}
);

module.exports = mongoose.model('data', dataSchema);