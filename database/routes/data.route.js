const express = require('express');
const router = express.Router();
const verify = require('./verifytoken');
const data_controller = require('../controllers/data.controller');

router.get('/test', verify, data_controller.test);
router.post('/inputuser', data_controller.user_create);
router.get('/listuser', data_controller.user_list);
router.get('/:id', verify, data_controller.user_one);
router.put('/:id/update', data_controller.user_update);
router.delete('/:id/delete', data_controller.user_delete);
router.post('/login', data_controller.user_login);


module.exports = router;
