import React from 'react';
import './Dashboard.css';
import axios from 'axios';

class Dashbo extends React.Component{
  constructor(props){
    super(props);
              
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event){    
    event.preventDefault();
        axios.get('http://localhost:8082/database/test')
        .then(
          console.log("sukses")
        ).catch((err)=>alert(err))
      }



    render (){
        return (
            <div className="container">
            <div className="bg1">
              <h2>16 <span>| 24</span></h2>
              <p>Goals Completed</p>
            </div>
            <div className="bg1">
              <h2><i className="fas fa-battery-three-quarters"></i></h2>
              <p>Respiration</p>
            </div>
            <div className="bg2">
              <h2><i className="fas fa-running"></i></h2>
              <p>Kilometers</p>
            </div>
            <div className="bg1">
              <h2>30 &deg;C</h2>
              <p>Temperature</p>
            </div>
            <div className="bg1">
              <h2><i className="fas fa-bed"></i></h2>
              <p>Sleep More</p>
            </div>
            <div className="bg2">
              <h2>98 <span>bpm</span></h2>
              <p>Heart Rate</p>
            </div>
            <div className="bg1">
              <h2>75 <span>Kg</span></h2>
              <p>Weight</p>
            </div>
            <div className="bg1">
              <h2>28 <span>%</span></h2>
              <p>Fat Percentage</p>
            </div>
            <div className="bg2">
              <h2>118 <span>mgdl</span></h2>
              <p>Blood Glucose</p>
            </div>
            <div className="bg2">
              <h2>680 <span>kcal</span></h2>
              <p>AVG Consumption</p>
            </div>
            <div className="bg2">
              <h2><i className="fas fa-dumbbell"></i></h2>
              <p>Workouts</p>
            </div>
            <div className="bg2" onClick={this.handleSubmit}>
              <h2>85 <span>%</span></h2>
              <p>Body Hydration</p>
            </div>
          </div>
    )
}
}

export default Dashbo;