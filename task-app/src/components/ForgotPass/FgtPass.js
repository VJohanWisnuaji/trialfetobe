import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import "../../custom.css";
import axios from 'axios';


const divStyle = {
    marginTop: '25px',
    marginBottom: '25px',
    backgroundColor: 'paleturquoise',
    padding: '25px'
    };

class FgtPass extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            email: "",
            password: "",
            confirmPassword: "",
            unQuest: "",
            unAns: ""
        }
        this.handleFormEmail = this.handleFormEmail.bind(this);
        this.handleFormPassword = this.handleFormPassword.bind(this);
        this.handleFormConPassword = this.handleFormConPassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleUnQuest = this.handleUnQuest.bind(this);
        this.handleUnAns = this.handleUnAns.bind(this)
        }
        
        handleFormEmail(event){
            console.log('Value', event.target.value);
            this.setState({email: event.target.value});
        }
        handleFormPassword(event){
            console.log('Value', event.target.value);
            this.setState({password: event.target.value});
        }
        handleFormConPassword(event){
            console.log('Value', event.target.value);
            this.setState({confirmPassword: event.target.value});
        }
        handleUnQuest(event){
            console.log('Value', event.target.value);
            this.setState({unQuest: event.target.value});
        }
        handleUnAns(event){
            console.log('Value', event.target.value);
            this.setState({unAns: event.target.value});
        }

        handleSubmit(event){    
            event.preventDefault();
            if(this.state.password === this.state.confirmPassword) {
                
                axios.get('http://localhost:8082/database/listuser')
                .then( async (res) => {
                    const findEmail = res.data.data.find((item)=>
                    {return item.email === this.state.email})
                    console.log(findEmail._id)
                    if (findEmail.unQuest === this.state.unQuest && findEmail.unAns === this.state.unAns) 
                    {
                        axios.put (`http://localhost:8082/database/${findEmail._id}/update`,{
                            email: this.state.email,
                            password: this.state.password,
                            confirmPassword: this.state.confirmPassword,
                        })
        
                        console.log(findEmail.unQuest)
                    } else { alert('Your pick a wrong question or write a wrong answer')}
            }).then (
                console.log('Success')).catch((err)=> alert("Email is not resgistered yet")
                );
            } else {
                alert("Confirmation password is wrong");
                console.log('Wrong'); 
            }
        }


    render (){
        return (
        <div className="card col-12 col-lg-4 login-card mt-2 hv-center">
            <Form style={divStyle} >
                
                {/* Input Email */}
                <div className="form-group text-left">
                    <label htmlFor="exampleInputEmail1">
                        Email address
                    </label>
                    <input  type="email"
                            className="form-control"
                            id="email"
                            aria-describedby="emailHelp"
                            placeholder= "Input Email"
                            onChange={this.handleFormEmail}
                            />
                </div>

                {/* Pick Unique Question */}
                <div className="form-group text-left">
                <Form.Group controlId="exampleForm.SelectCustomSizeSm">
                    <Form.Label>Pick your unique question</Form.Label>
                    <Form.Control as="select" size="lg" custom onChange={this.handleUnQuest}>
                    <option>...</option>
                    <option>What is your first pet name?</option>
                    <option>What is your favorite color?</option>
                    <option>Who is your bestfriend?</option>
                    <option>Why are you alive?</option>
                    <option>What is your favorite book?</option>
                    </Form.Control>
                </Form.Group>
                </div>

                {/* Input Unique Answer */}
                <div className="form-group text-left">
                <label htmlFor="exampleUsername">
                        Your unique answer
                    </label>
                <input  type="text"
                            className="form-control"
                            id="unAns"
                            placeholder= "Type your unique answer here"
                            onChange={this.handleUnAns}
                            />
                </div>

                {/* Input Password */}
                <div className="form-group text-left">
                    <label htmlFor="exampleInputPassword1">
                    New Password
                    </label>
                    <input type="password"
                        className="form-control"
                        id="password"
                        placeholder="Input your new password"
                        onChange={this.handleFormPassword}
                    />
                </div>

                {/* Confirm Password */}
                <div className="form-group text-left">
                    <label htmlFor="exampleInputConPassword1">
                    Confirm New Password
                    </label>
                    <input type="password"
                        className="form-control"
                        id="confirmPassword"
                        placeholder="Confirm your new password"
                        onChange={this.handleFormConPassword}
                    />
                </div>
                
                {/* Button */}
                <div className="row">
                    <div className="col"></div>
                    <div className="col"> 
                                <Button type="submit" variant="success"
                                    className="btn btn-primary justify-center"
                                    onClick= {this.handleSubmit}
                                    >
                                    Renew
                                </Button> </div>
                    <div className="col"> </div>
                    </div>

            </Form>
        </div>
    )
}
}

export default FgtPass;