import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { NavLink } from 'react-router-dom';
import axios from 'axios';

const divStyle = {
    marginTop: '25px',
    marginBottom: '25px',
    backgroundColor: 'paleturquoise',
    padding: '25px'
    };

class LoForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            email: "",
            password: ""
        }
        this.handleFormEmail = this.handleFormEmail.bind(this);
        this.handleFormPassword = this.handleFormPassword.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleRegister = this.handleRegister.bind(this);
        this.handleFgtPass = this.handleFgtPass.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        }
        
        handleFormEmail(event){
            console.log('Value', event.target.value);
            this.setState({email: event.target.value});
        }
        handleFormPassword(event){
            console.log('Value', event.target.value);
            this.setState({password: event.target.value});
        }
        handleLogin(event){    
            event.preventDefault();
            console.log('Wrong'); 
        }

        handleRegister(event){    
            event.preventDefault();
            console.log('Wrong'); 
        }

        handleFgtPass(event){    
            event.preventDefault();
            console.log('Wrong'); 
        }

        handleSubmit(event){    
            event.preventDefault();
                axios.post('http://localhost:8082/database/login',
                {
                email: this.state.email,
                password: this.state.password
                }
                )
                .then (
                (res)=>(
                window.location.href='http://localhost:3000/dash',
                console.log(res.data),    
                console.log("email", this.state.email,
                "password", this.state.password),
                console.log('Success')))
                
                .catch((err)=> alert(err)
                );
            }

    render (){
        return (
        <div className="card col-12 col-lg-4 login-card mt-2 hv-center">
            <Form style={divStyle}>

                {/* Input Email */}
                <div className="form-group text-left">
                    <label htmlFor="exampleInputEmail1">
                        Email address
                    </label>
                    <input  type="email"
                            className="form-control"
                            id="email"
                            aria-describedby="emailHelp"
                            placeholder= "Input Email"
                            onChange={this.handleFormEmail}
                            />
                </div>

                {/* Input Password */}
                <div className="form-group text-left">
                    <label htmlFor="exampleInputPassword1">
                    Password
                    </label>
                    <input type="password"
                        className="form-control"
                        id="password"
                        placeholder="Input Password"
                        onChange={this.handleFormPassword}/>
                </div>

                {/* Button */}
                <div className="row">
    <div className="col"></div>
            <div className="col"> 
                <Button variant="success"
                    className="btn btn-primary justify-center" onClick={this.handleSubmit}
                >
                    Login
                    
                </Button>
            </div>
    <div className="col"> </div>
    
    </div>
    
    {/* LINK */}
    <NavLink to='/register'><small id="emailHelp" className="form-text text-muted">You don't have an account yet? click here to register</small></NavLink>
    
    <NavLink to='/fgtpass'><small id="emailHelp" className="form-text text-muted">Forgot password? click here</small></NavLink>
            </Form>
        </div>
    )
}
}

export default LoForm;