import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import axios from 'axios';



const divStyle = {
    marginTop: '25px',
    marginBottom: '25px',
    backgroundColor: 'paleturquoise',
    padding: '25px'
    };



class RegisForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            email: "",
            password: "",
            confirmPassword: "",
            fullName: "",
            username: "",
            unQuest: "",
            unAns: ""
        }
        this.handleFormEmail = this.handleFormEmail.bind(this);
        this.handleFormPassword = this.handleFormPassword.bind(this);
        this.handleFormConPassword = this.handleFormConPassword.bind(this);
        this.handleFullName = this.handleFullName.bind(this);
        this.handleusername = this.handleusername.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleUnQuest = this.handleUnQuest.bind(this);
        this.handleUnAns = this.handleUnAns.bind(this)
        }
        
        handleFormEmail(event){
            console.log('Value', event.target.value);
            this.setState({email: event.target.value});
        }
        handleFormPassword(event){
            console.log('Value', event.target.value);
            this.setState({password: event.target.value});
        }
        handleFormConPassword(event){
            console.log('Value', event.target.value);
            this.setState({confirmPassword: event.target.value});
        }
        handleFullName(event){
            console.log('Value', event.target.value);
            this.setState({fullName: event.target.value});
        }
        handleusername(event){
            console.log('Value', event.target.value);
            this.setState({username: event.target.value});
        }
        handleUnQuest(event){
            console.log('Value', event.target.value);
            this.setState({unQuest: event.target.value});
        }
        handleUnAns(event){
            console.log('Value', event.target.value);
            this.setState({unAns: event.target.value});
        }

        handleSubmit(event){    
            event.preventDefault();
            if(this.state.password === this.state.confirmPassword) {
                axios.post ('http://localhost:8082/database/inputuser',{
                    email: this.state.email,
                    password: this.state.password,
                    confirmPassword: this.state.confirmPassword,
                    fullName: this.state.fullName,
                    username: this.state.username,
                    unQuest: this.state.unQuest,
                    unAns: this.state.unAns
                })
                console.log(
                    "email", this.state.email,
                    "password", this.state.password,
                    "confirmPassword:", this.state.confirmPassword,
                    "fullName:", this.state.fullName,
                    "userName:", this.state.username,
                    "unQuest", this.state.unQuest,
                    "unAns:", this.state.unAns
                );
                console.log('Success');
            } else {
                alert("Confirmation password is wrong");
                console.log('Wrong'); 
            }
        }


    render (){
        return (
        <div className="card col-12 col-lg-4 login-card mt-2 hv-center">
            <Form style={divStyle}>
                
                {/* Input Email */}
                <div className="form-group text-left">
                    <label htmlFor="exampleInputEmail1">
                        Email address
                    </label>
                    <input  type="email"
                            className="form-control"
                            id="email"
                            aria-describedby="emailHelp"
                            placeholder= "Input Email"
                            onChange={this.handleFormEmail}
                            />
                    <small id="emailHelp" className="form-text text-muted">We'll never share</small>
                </div>

                {/* Input Full Name */}
                <div className="form-group text-left">
                <label htmlFor="exampleFullName">
                        Full Name
                    </label>
                <input  type="text"
                            className="form-control"
                            id="fullName"
                            placeholder= "Type your full name"
                            onChange={this.handleFullName}
                            />
                </div>

                {/* Input Username */}
                <div className="form-group text-left">
                <label htmlFor="exampleUsername">
                        Username
                    </label>
                <input  type="text"
                            className="form-control"
                            id="nickName"
                            placeholder= "Type your personal username"
                            onChange={this.handleusername}
                            />
                </div>

                {/* Pick Unique Question */}
                <div className="form-group text-left">
                <Form.Group controlId="exampleForm.SelectCustomSizeSm">
                    <Form.Label>Pick your unique question</Form.Label>
                    <Form.Control as="select" size="lg" custom onChange={this.handleUnQuest}>
                    <option>...</option>
                    <option>What is your first pet name?</option>
                    <option>What is your favorite color?</option>
                    <option>Who is your bestfriend?</option>
                    <option>Why are you alive?</option>
                    <option>What is your favorite book?</option>
                    </Form.Control>
                </Form.Group>
                </div>

                {/* Input Unique Answer */}
                <div className="form-group text-left">
                <label htmlFor="exampleUsername">
                        Your unique answer
                    </label>
                <input  type="text"
                            className="form-control"
                            id="unAns"
                            placeholder= "Type your unique answer here"
                            onChange={this.handleUnAns}
                            />
                </div>

                {/* Input Password */}
                <div className="form-group text-left">
                    <label htmlFor="exampleInputPassword1">
                    Password
                    </label>
                    <input type="password"
                        className="form-control"
                        id="password"
                        placeholder="Input Password"
                        onChange={this.handleFormPassword}
                    />
                </div>

                {/* Confirm Password */}
                <div className="form-group text-left">
                    <label htmlFor="exampleInputConPassword1">
                    Confirm Password
                    </label>
                    <input type="password"
                        className="form-control"
                        id="confirmPassword"
                        placeholder="Confirm Password"
                        onChange={this.handleFormConPassword}
                    />
                </div>
                
                {/* Button */}
                <div className="row">
                    <div className="col"></div>
                    <div className="col"> 
                                <Button type="submit" variant="success"
                                    className="btn btn-primary justify-center"
                                    onClick= {this.handleSubmit}
                                    >
                                    Register
                                </Button> </div>
                    <div className="col"> </div>
                    </div>

            </Form>
        </div>
    )
}
}

export default RegisForm;