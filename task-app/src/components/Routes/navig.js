import React from 'react';
import { NavLink } from 'react-router-dom';

const Navigation = () => (

    <nav>
    <ul>
      <li><NavLink to='/'>Home</NavLink></li>
      <li><NavLink to='/register'>About</NavLink></li>
      <li><NavLink to='/fgtpass'>Contact</NavLink></li>
    </ul>
  </nav>
  
  );

  export default Navigation;