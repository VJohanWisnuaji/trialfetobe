import React from 'react';
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Page3 from './page3';
import Page2 from './page2';
import Page1 from './page1';
import Page4 from './page4';

function Main() {
  return (
    <Router>
    <div>
    <Switch>
    <Route path='/' exact={true}><Page1 /></Route>
    <Route path='/register'><Page2 /></Route>
    <Route path='/fgtpass'><Page3 /></Route>
    <Route path='/dash'><Page4 /></Route>
    </Switch>
    </div>
    </Router>
  );
  }

export default Main;