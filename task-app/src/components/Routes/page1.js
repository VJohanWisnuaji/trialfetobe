import React from 'react';
import HeLog from '../Header/HeLog';
import LoForm from '../LoginForm/LoForm';
import { withRouter } from "react-router-dom";


const divStyle = {
    marginTop: '15px',
    marginBottom: '25px'
    };



function Page1() {
    return (
    <div>
        <HeLog />
        <div className="containergg d-flex align-items-center flex-column"  style={divStyle} >
        <LoForm />
        </div>
    </div>
    );
}

export default withRouter(Page1);
