import React from 'react';
import Header from '../Header/Header';
import RegisForm from '../RegistrationForm/Regis';
import { withRouter } from "react-router-dom";

const divStyle = {
    marginTop: '15px',
    marginBottom: '25px'
    };



function Page2() {
    return (
    <div>
        <Header />
        <div className="containergg d-flex align-items-center flex-column" style={divStyle}>
        <RegisForm />
        </div>
    </div>
    );
}

export default withRouter(Page2);
