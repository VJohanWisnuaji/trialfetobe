import React from 'react';
import HeFgt from '../Header/HeFgt';
import FgtPass from '../ForgotPass/FgtPass';
import { withRouter } from "react-router-dom";

const divStyle = {
    marginTop: '15px',
    marginBottom: '25px'
    };



function Page3() {
    return (
    <div>
        <HeFgt />
        <div className="containergg d-flex align-items-center flex-column" style={divStyle}>
        <FgtPass />
        </div>
    </div>
    );
}

export default withRouter(Page3);
