import React from 'react';
import { withRouter } from "react-router-dom";
import HeDash from '../Header/Hedash';
import Dashbo from '../Dashboard/Dashboard';


const divStyle = {
    marginTop: '15px',
    marginBottom: '25px'
    };



function Page4() {
    return (
    <div>
        <HeDash />
        <div style={divStyle} >
        <Dashbo />
        </div>
    </div>
    );
}

export default withRouter(Page4);
